# CosmoApp

Application to manage cosmonauts (personal project for a job interview)

## CosmoServer(Backend)

Backend REST server for retrievin cosmonauts from MongoDB 

Technologies used: Node.js, Express, MongoDB

## CosmoManager(Frontend)

Frontend application with UI to manage cosmonauts

Technologies used: AngularJS, Bower, Grunt, Sass(BEM)

## Run

Server - sudo mongod, nodemon

Manager - npm start