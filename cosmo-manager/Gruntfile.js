module.exports = function(grunt) {
  grunt.initConfig({
    sass: {
      dist: {
        files: {
          'app/app.css': 'app/app.scss'
        }
      }
    },
    autoprefixer: {
      options: {
        browsers: ['last 5 versions']
      },
      files: {
        'app/app.css': 'app/app.css'
      }
    },
    cssmin: {
      options: {
        sourcemap: true
      },
      target: {
        files: {
          'app/deploy/app.min.css': 'app/app.css'
        }
      }
    },
    watch: {
      files: ['app/app.scss'],
      tasks: ['sass','autoprefixer','cssmin']
    }
  });

  grunt.loadNpmTasks('grunt-contrib-sass');
  grunt.loadNpmTasks('grunt-contrib-cssmin');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-autoprefixer');
  grunt.registerTask('default', ['sass','autoprefixer','cssmin']);
};