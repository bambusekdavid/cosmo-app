'use strict';

// Declare app level module which depends on views, and components
angular.module('cosmoManager', [
  'ngRoute',
  'ngResource',
  'jkuri.datepicker',
  'cosmoManager.index',
  'cosmoManager.new',
  'cosmoManager.services'
]).
config(['$locationProvider', '$routeProvider', function($locationProvider, $routeProvider) {
  $locationProvider.hashPrefix('!');

  $routeProvider.otherwise({redirectTo: '/index'});
}]);
