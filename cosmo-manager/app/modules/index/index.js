'use strict';

angular.module('cosmoManager.index', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/index', {
    templateUrl: 'modules/index/index.html',
    controller: 'IndexCtrl'
  });
}])

.controller('IndexCtrl', ['$rootScope','$scope','$routeParams','cosmonautService',function($rootScope,$scope,$routeParams,cosmonautService) {
  $rootScope.tab = 'index';
  $scope.flash = $routeParams.flash;

  $scope.getAllPosts = function(){
    cosmonautService.getAll().success(function(data){
    	$scope.cosmonauts = data;
    });
  };

  $scope.deleteModal = function(cosmoID){
    $scope.deleteID = cosmoID;
  };

  $scope.deleteCosmonaut = function(cosmoID){
    cosmonautService.deleteCosmonaut(cosmoID).success(function(){
      console.log("Cosmonaut vymazán");
      $scope.getAllPosts();
      $scope.modal = false;
      $scope.flash = false;
    });
  }

  $scope.getAllPosts();
  
  if($routeParams.flash == 'new'){
    $scope.flash = true;
  }

}]);