'use strict';

angular.module('cosmoManager.new', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/new', {
    templateUrl: 'modules/new/new.html',
    controller: 'NewCtrl'
  });
}])

.controller('NewCtrl', ['$rootScope','$scope','$location','cosmonautService',function($rootScope,$scope,$location,cosmonautService) {
	$rootScope.tab = 'new';
	$scope.superpowers = ["rentgenové vidění","čtení myšlenek","nesmrtelnost","bezedný pivní břuch","frontend vývojář"];
	$scope.cosmonaut = {};

	$scope.saveCosmonaut = function(){
		if($scope.newcosmonaut.$valid){
			console.log("OK");
			cosmonautService.saveCosmonaut($scope.cosmonaut).success(function(){
				$rootScope.tab = 'index';
				$location.path( "/index" ).search('flash=new')
			});
		} else {
			console.log("Error");
		}
	};

	$scope.changeSuperpower = function(dir,sp){
		var sp_len = $scope.superpowers.length;
		var index = $scope.superpowers.indexOf(sp);
		
		index += dir;
		
		if(index < 0){
			index = sp_len - 1;
		}
		if(index == sp_len){
			index = 0;
		}
		$scope.cosmonaut.superpower = $scope.superpowers[index];
	}
	
}]);