angular.module('cosmoManager.services',['ngResource']).factory('cosmonautService',['$http',function($http){
	return{
		getAll: function(){
			return $http.get('http://localhost:3000/cosmonauts')
		},
		getCosmonautById: function(id){
			for(var i in this.posts){
				if(this.posts[i].id == id){
					return this.posts[i];
				}
			}
		},
		saveCosmonaut: function(cosmonaut){
			return $http.post('http://localhost:3000/cosmonauts', cosmonaut);
		},
		deleteCosmonaut: function(cosmoId){
			return $http.delete('http://localhost:3000/cosmonauts/'+cosmoId);
		}
	}
}]);