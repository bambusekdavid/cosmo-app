var mongoose = require('mongoose');
var CosmonautSchema = new mongoose.Schema({
  name: String,
  surname: String,
  dob: String,
  superpower: String,
});
module.exports = mongoose.model('Cosmonauts', CosmonautSchema);