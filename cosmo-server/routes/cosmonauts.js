var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var Cosmo = require('../models/Cosmoserver.js');

/* GET */
router.get('/', function(req, res, next) {
  Cosmo.find(function (err, todos) {
    if (err) return next(err);
    res.json(todos);
  });
});
module.exports = router;

/* POST */
router.post('/', function(req, res, next) {
  Cosmo.create(req.body, function (err, post) {
    if (err) return next(err);
    res.json(post);
  });
});

/* GET */
router.get('/:id', function(req, res, next) {
  Cosmo.findById(req.params.id, function (err, post) {
    if (err) return next(err);
    res.json(post);
  });
});

/* DELETE*/
router.delete('/:id', function(req, res, next) {
  Cosmo.findByIdAndRemove(req.params.id, req.body, function (err, post) {
    if (err) return next(err);
    res.json(post);
  });
});